<!-- Footer class -->
    <footer class="site-footer">
      <div class="site-footer__inner container container--narrow">
        <div class="group">
          <div class="site-footer__col-one">
            <h1 class="unitee-logo-text unitee-logo-text--alt-color">
              <a href="<?php echo site_url()?>"><strong>Unitee</strong></a>
            </h1>
            <!-- Link later, Unitee responsibility -->
            <p><a class="site-footer__link" href="#">UniteeHelp@outlook.com</a></p>
          </div>

          <div class="site-footer__col-two-three-group">
            <div class="site-footer__col-two">
              <h3 class="headline headline--small">Explore</h3>
              <nav class="nav-list">
                <ul>
                  <!-- WordPress functions to get page links -->
                  <li><a href="<?php echo site_url('/shop')?>">Store</a></li>
                  <li><a href="<?php echo site_url('/about-us')?>">About-Us</a></li>
                  <li><a href="<?php echo site_url('/my-account')?>">Account</a></li>
                </ul>
              </nav>
            </div>

            <div class="site-footer__col-three">
              <h3 class="headline headline--small">Legal</h3>
              <nav class="nav-list">
                <ul>
                  
                  <li><a href="<?php echo site_url('/privacy-policy')?>">Privacy</a></li>
                  
                </ul>
              </nav>
            </div>
          </div>

          <!-- Social media icons - facebook, twitter, youtube, instagram. icons taken from font awesome -->
          <!-- On big screens present in column 4 footer, on mobile devices located under footer links -->
          <div class="site-footer__col-four">
            <h3 class="headline headline--small">Connect With Us</h3>
            <nav>
              <ul class="min-list social-icons-list group">
                <!-- Social media links - Unitee Responsibility -->
                <li>
                  <a href="#" class="social-color-facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                </li>
                 <li>
                  <a href="#" class="social-color-twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                </li> 
                 <li>
                  <a href="#" class="social-color-youtube"><i class="fab fa-youtube" aria-hidden="true"></i></a>
                </li>
                <li>
                  <a href="#" class="social-color-instagram"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </footer> 

 
<!-- Search overlay - When search icon is clicked, overlay is added over header. User can then search for products with key words i.e animal name -->
<!-- Same background image as header - cloud illustration -->
<div class="search-overlay" style="background-image: url(<?php echo get_theme_file_uri('/images/clouds.png')?>);">
    <div class="search-overlay__top">
      <div class="container">
      <h1 class="unitee-logo-text search-overlay-title float-left">
          <!-- Website Header, Logo Unitee Responsibility -->
          <a href="<?php echo site_url()?>">Unitee</a>
        </h1>
        <!-- <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i> -->
        <!-- text field for user to enter their search in -->
        <?php echo do_shortcode('[fibosearch]'); ?>
        <!-- <input type="text" class="search-term" autocomplete="off" placeholder="What are you looking for?" id="search-term"> -->
        <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
      </div>
    </div>
  </div>
  <?php wp_footer();?>
</body>

  <script type="text/javascript">
    jQuery(document).ready(function () {

        var width = jQuery("#child").get(0).scrollWidth;
        var halfWidth = width / 2;

        var height = jQuery("#child").get(0).scrollHeight;
        var halfHeight = height / 4;

        if (jQuery(window).width() > 760) {
          // On desktops > 760 px make whole picture dragable left right up down
            jQuery("#child").draggable({
                cursor: "move",
                // containment: "parent",
                // draggable rules
                stop: function () {
                    if (jQuery("#child").position().left > 1)
                        jQuery("#child").css("left", "0px")

                    if (jQuery("#child").position().top > 1)
                        jQuery("#child").css("top", "0px")

                    if ((jQuery("#child").position().left) * (-1) > halfWidth)
                        jQuery("#child").css("left", '-' + halfWidth + "px")

                    if ((jQuery("#child").position().top) * (-1) > halfHeight)
                        jQuery("#child").css("top", '-' + halfHeight + "px")
                }
            });
            jQuery(".zooSection").css("overflow-y", "hidden");
        }
        else {
          // smaller screens can just scroll left and right
          jQuery(".zooSection").css("overflow", "auto !important");
        }
        // Make page load viewing middle of illustration
        jQuery(".zooSection").scrollLeft(width / 2.5 - width * 0.145);
        jQuery$('html, body').animate({ scrollTop: '0px' }, 300);

        jQuery(window).resize(function () {
            window.location.reload();
        });
    });
  </script>


</html>