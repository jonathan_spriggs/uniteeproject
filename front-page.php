<?php
// if you need help with functions codex.wordpress.org. developer.wordpress.org

get_header();
?>
<!-- Position relative so clouds are positioned correctly on different screen sizes -->
<div class="padding-top " style="position:relative">
<!-- 3 Clouds animated in CSS to go from left to right side of screen in the background. Animated at various speeds and positions -->
<div style="overflow:hidden" id="movingClouds">
<!-- Each cloud is different size/shape -->
	<img class="CloudsImage" src="<?php echo get_theme_file_uri('images/MovingClouds3.png') ?>"/>
</div>
<div id="movingClouds2">
	<img class="CloudsImage" src="<?php echo get_theme_file_uri('images/MovingClouds4.png') ?>"/>
</div>
<div id="movingClouds3">
	<img class="CloudsImage" src="<?php echo get_theme_file_uri('images/MovingClouds6.png') ?>"/>
</div>

<!-- SVG responsive image map. Was tried with HTML image map however was not possible to get hover effect -->
<!-- PNG image converted to SVG. Each path represents the different boards in the image. Each board in the image goes to a seperate link -->
<!-- The City and The Wild Boards currently link to Zoo illustration - Once Unitee provide me with City and Wild illustration links will be updated -->
  <figure id="projectsvg">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     viewBox="0 0 3155 2048" preserveAspectRatio="xMinYMin meet" >
  <!-- set your background image -->
  <image width="3155" height="2048" xlink:href="<?php echo get_theme_file_uri('/images/homescreen3.png')?>"/>
  <!-- Using Opacity and fill to create different shade on hover -->
  <g class="hover_group" opacity="0">
    <a xlink:href="<?php echo site_url('/zoo')?>">
      <path opacity="0.15" fill="#006400" d="m265.39 371.55 93.091-62.877 152.7-70.227 93.091-29.397 83.292-2.4498 462.19-14.699 980.72-2.4498 76.759 4.083 42.463 8.1659 43.279 17.148 17.148 29.397 8.1658 39.196 8.1659 131.47 0.8166 164.95-4.8995 57.161-13.065 20.415-177.2 12.249-398.5 4.8995-498.94 1.6332-543.85 0.81658-51.445 2.4498-13.882 0.81659-92.275-48.179-209.05-155.15-72.676-84.925z" />
    </a>
  </g>
  <g class="hover_group" opacity="0">
  <a xlink:href="<?php echo site_url('/zoo')?>">
    <path opacity="0.15" fill="#535353" d="m1295.1 741.46 645.92 0.81659 242.53 27.764 49.812 2.4498 54.711-4.0829 66.144-8.1659 39.196-4.0829 75.943-0.81659 62.877 3.2664 40.013 7.3493 22.048 11.432 13.066 19.598 9.799 26.947 13.882 62.061 4.8996 102.89 23.681-2.4498 53.895 12.249 17.965 24.498 7.3493 49.812 21.231 123.3-8.1658 62.061-15.515 34.297-26.948 24.498-40.013 7.3493-147.8-9.7991-71.86-20.415-13.882-32.664-1.6332-25.314-7.3493-30.214-338.07 7.3493-12.249 1.6331 3.2664 13.882-25.314 22.864-88.192 4.0829-504.65-4.0829-34.297-4.8996-18.782-22.048-0.8166-31.847-22.864-57.978 9.7991-22.864-7.3493-10.616-34.297 31.847-49.812 5.7161-43.279-14.699-40.013-52.262v-75.126l-71.043-24.498-16.332-27.764-8.1659-106.16 4.8996-50.629 27.764-31.03z"/>
  </a>
  </g>
  <g class="hover_group" opacity="0">
  <a xlink:href="<?php echo site_url('/about-us')?>">
    <path opacity="0.1" fill="#bdddf2" d="m142.09 805.16-13.065 48.179-4.8995 257.23 10.616 62.061 15.515 18.782 31.03 11.432 338.07 3.2664 422.99-6.5328 22.048-13.065 8.1659-32.664-6.5327-329.9-19.598-17.965-48.179-8.9825-137.19 2.4498-3.2664 62.877-8.1659 19.598-25.314-13.065-7.3493-75.126h-342.97l-3.2664 66.96-13.882 16.332-18.782-25.314-3.2664-26.947-3.2664-30.214-155.15-1.6332z"/>
  </a>
  </g>
  <g class="hover_group" opacity="0">
  <a xlink:href="<?php echo site_url('/zoo')?>">
    <path opacity="0.1" fill="#9a7c0e" d="m801.89 1343.3c0 5.7161 31.847 13.882 31.847 13.882l33.48-19.598 71.043 21.231 63.694-8.1659 142.09 0.8165 33.48-26.131 20.415 13.065 111.06-14.699 65.327 9.7991 71.86-12.249 70.227 16.332 66.96-17.148 87.375 24.498 63.694-12.249 66.144 26.131 86.558-11.432 85.742 20.415 88.192-23.681 76.759 4.8996 58.794-17.965 99.624 15.515 63.694-4.0829 48.995 0.8166 31.847 38.38-4.8995 63.694 8.9825 53.895 49.812-0.8166 22.048 26.948 18.782 8.9824 1.6332 114.32-8.9825 21.231-6.5327 164.95 8.1659 89.825 22.864 65.327 39.196 35.113-8.1659 15.515-28.581 1.6332-49.812-60.428-19.598-88.192 2.4497-209.05-26.947 1.6332-5.7162 63.694-27.764 46.546-127.39 9.7991-350.32-4.8996-860.69-8.9824-57.161-26.131-13.065-51.445v-47.362l-126.57-4.0829-40.829-30.214-0.81659-110.24-44.096-13.882-31.03-38.38z"/>
  </a>
  </g>
  </svg>  
  </figure>

</div>
<?php
get_footer();
?>

  