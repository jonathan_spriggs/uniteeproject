import "../css/style.css"

// Our modules / classes
import MobileMenu from "./modules/MobileMenu"
import Search from "./modules/Search"
import LoginRegister from "./modules/LoginRegister"
import Preloader from "./modules/preloader"
// import Zoo from "./modules/zoo"


// Instantiate a new object using our modules/classes
const mobileMenu = new MobileMenu()
const magicalSearch = new Search()
const loginRegister = new LoginRegister()
// const preloader = new Preloader()
// const zoo = new Zoo()

// Allow new JS and CSS to load in browser without a traditional page refresh
if (module.hot) {
  module.hot.accept()
}
