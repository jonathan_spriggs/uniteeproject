import $ from "jquery"

class LoginRegister {
  // 1. describe and create/initiate our object
  constructor() {
    // Assign all important elements to variables in constructor
    this.login = $(".account-links__login")
    this.register = $(".account-links__register")

    this.loginForm = $(".woocommerce form.login")
    this.registerForm = $(".woocommerce-form-register")

    this.events()
  }

  // 2. events
  events() {

   this.login.on("click", this.clickLogin.bind(this))
   this.register.on("click", this.clickRegister.bind(this))

  }

  // 3. methods (function, action...)

  clickLogin(){
    // When click on login, assign inactive class to woocommerce register form - making it dissapear

    // If woocommerce log in has inactive class - remove it making it visible again
    this.loginForm.removeClass("inactive")
    this.registerForm.addClass("inactive")
  }
  
  clickRegister(){
    // Vice versa for when clicking on register
    this.loginForm.addClass("inactive")
    this.registerForm.removeClass("inactive")
  }

}

export default LoginRegister