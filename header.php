<!DOCTYPE html>
<!-- determines language of the site -->
<html <?php language_attributes();?>>
    <head>
    <!-- <script type="text/javascript" src="<?php get_stylesheet_directory_uri() . '/js/jquery.js'?>"></script>
    <script type="text/javascript" src="<?php get_stylesheet_directory_uri() . '/js/scrol.js'?>"></script> -->
    
    <script>
       document.addEventListener("DOMContentLoaded", function (event){
         const loadingWrapper = document.querySelector('.preload-wrapper');
         const seenAnimation = Cookies.get('seenAnimation');

       if(!seenAnimation){
         
          loadingWrapper.style.display ="flex";

          setTimeout(()=>{
            loadingWrapper.style.display ="none";
          },10000);
          Cookies.set('seenAnimation', 1, {expires: 1});
       }else{
        loadingWrapper.classList.add("displayNone");
        //  loadingWrapper.classList.add("displayNone");
      }
    });
    </script>

    <!-- Creates character set of UTF-8 as we living in england -->
    <meta charset="<?php bloginfo('charset');?>">
    <!-- to help with making pages responsive on phones etc -->
    <meta name ="viewport" content="width=device-width, initial-scale =1">
        <?php wp_head(); ?>
    </head>
     <!-- body_class() Gives you neat information/class names about the current page your viewing, you can use the class names in css to style particular page in specific way, or Javascript  -->
    <body <?php body_class();?> class="background-color:red;">
    <!-- Unitee header illustration as background image -->
    
    <!-- Preload wrapper - Animation displayed to users when page hasnt loaded yet - faded out when page loads - through javascript -->
    <!-- Animation includes bird flying with 3 clouds shooting past -->
    
   
    <header class="site-header " style="background-image: url(<?php echo get_theme_file_uri('/images/clouds.png')?>);">
    <!-- preload wrapper in the header class to load quicker -->
    <div style="display:none" class="preload-wrapper">
      <div class="preload">
        <img class="bird" src = "<?php echo get_theme_file_uri('/images/Bird2.png')?>" alt="loadingImageBird"/> 
          <h3> Welcome to the website...</h3>
          <img class="cloud1" src="<?php echo get_theme_file_uri('/images/MovingClouds.png')?>" alt="loadingImageCloud"/>
          <img class="cloud2" src="<?php echo get_theme_file_uri('/images/MovingClouds.png')?>" alt="loadingImageCloud"/> 
          <img class="cloud3" src="<?php echo get_theme_file_uri('/images/MovingClouds.png')?>" alt="loadingImageCloud"/> 
      </div>
      </div>
      <div class="container">
        <h1 class="unitee-logo-text float-left">
          <!-- Website Header, Logo Unitee Responsibility -->
          <a href="<?php echo site_url()?>">Unitee</a>
        </h1>
        <!-- Search Icon - Icon taken from font awesome -->
        <span class="js-search-trigger site-header__search-trigger"><i class="fa fa-search" aria-hidden="true"></i></span>
        <!-- Area hidden = True, removes element from accessibility tree - assistive technology users -->
        <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
        <div class="site-header__menu group">
          <nav class="main-navigation">
          
             <ul> 
             <!-- As its a unique website we keep the navigation menu here, if we were going to create a generic theme we wouldnt do this --> 
             <!-- wp_get_post_parent_id(0) - 0 in parenthesis means parent page of current page -->

             <li><a href="<?php echo site_url('/zoo')?>">Zoo</a></li>
              <li><a href="<?php echo site_url('/shop')?>">Store</a></li>
              <li><a href="<?php echo site_url('/about-us')?>">About-Us</a></li>
              <li><a href="<?php echo site_url('/my-account')?>">Account</a></li>
              <li><a href="<?php echo site_url('/cart')?>">Cart</a></li>
             
            </ul> 
          </nav>
          <!-- <div class="site-header__util">
        <span class="search-trigger js-search-trigger"><i class="fa fa-search" aria-hidden="true"></i></span> 
        </div> -->
        </div>
      </div>
    </header>


     