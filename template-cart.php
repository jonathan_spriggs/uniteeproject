
<?php
  // Template Name: Cart Page
  get_header();
?>

<div style="display: block; overflow:auto; background-color:#9dc3e6;position:relative">

<!-- Background image illustration designed by me. Only background illustration designed by me before i decided to make use of Unitee Landscapes already created in designs -->
<div class="page-backgrounds__cart" style="background-image: url(<?php echo get_theme_file_uri('/images/CheckoutBackground.png')?>); padding-top:200px;" >
<!-- Slow moving cloud - animated from left to right side of screen. Parent div positioned relative to help with cloud positioning on smaller screens -->

<div id="moving-clouds-cart">
	<img class="CloudsImage" src="<?php echo get_theme_file_uri('images/MovingClouds4.png') ?>" alt="movingCloud"/>
</div>
<!-- Implement woocommerce content - cart and order detail fill out. All my custom css is in woocommerce.css file -->
      <div style="padding-top:50px;" class="checkout-page__products container">
          <?php the_content();?>
      </div>
  </div>
</div>
<?php 
  get_footer();
?>
