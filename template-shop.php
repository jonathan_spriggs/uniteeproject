


<?php
// Template Name: Shop Page

get_header();
 ?>

<!-- WooCommerce Shop page -->
<!-- Background taken from a Unitee design -->
<div class="checkout-page" style="background-image: url(<?php echo get_theme_file_uri('/images/CheckoutBackground.png')?>); padding-top:200px;" >

    <div style="padding-top:50px;" class="checkout-page__products container">
        <?php the_content();?>
    </div>
    
</div>

<?php 
  get_footer();
?>
