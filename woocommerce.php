<?php
get_header();?>


<!-- WooCommerce Shop page -->
<!-- Background taken from a Unitee design -->

<div style="background-image: url(<?php echo get_theme_file_uri('/images/ShopBackground3.png')?>);padding-top:13rem " class="privacy-policy">
<div style="text-align:center" class ="container ">  
<!-- Currenlty not using - Shop banner illustration -->
<!-- <img class="page-headers__shop container" src ="<?php echo get_theme_file_uri('/images/ShopBanner.png')?>"/>   -->
</div>

<div style="padding-top:20px" class ="container">

<?php 
    // Shortcodes for implementing category filter. Filter created with a plugin - Shortcodes Ultimate
    echo do_shortcode("[searchandfilter id='categories']");

    // Shortcode for backbutton - custom css made so only visible in single product page
    echo do_shortcode("[alg_back_button]");

    // Custom woocommerce css found in woocommerce.css file. Also core files in woocommerce folder were changed
    woocommerce_content();?>
</div>

</div>

<?php
get_footer();?>

