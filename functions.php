<?php 
function unitee_files(){
    
    wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.6.1/css/all.css' );

    
    wp_enqueue_script('our-main-styles', get_theme_file_uri('/bundled-assets/styles.6861aad0937d8d25dec3.css'));
    // if current domain wordpress is running on = unitee.local then running locally on computer in private sandbox
    
    //   if(strstr($_SERVER['SERVER_NAME'],'unitee.local')){
    //       wp_enqueue_script('unitee-javascript','http://localhost:3000/bundled.js', NULL, '1.0',true);
    //  }else {
        wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.c769d22454e31058a207.js'), NULL, '1.0', true);
        wp_enqueue_script('main-university-js', get_theme_file_uri('/bundled-assets/scripts.6861aad0937d8d25dec3.js'), NULL, '1.0', true);
        wp_enqueue_style('our-main-styles', get_theme_file_uri('/bundled-assets/styles.6861aad0937d8d25dec3.css'));
//    }    
}


// Adding java script files associated to making Zoo work
function zoo_javascript(){
    wp_enqueue_script('zoo', get_stylesheet_directory_uri() . '/js/modules/Zoo.js', false, 1.0, false );
    wp_enqueue_script('j-query', get_stylesheet_directory_uri() . '/js/jquery.js', false, 1.0, false );
    wp_enqueue_script('scrol', get_stylesheet_directory_uri() . '/js/scrol.js', false, 1.0, false );
    wp_enqueue_script( 'cookies', 'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js', false,1.0,false);
   
}

add_action('wp_enqueue_scripts', 'zoo_javascript');

add_action('wp_enqueue_scripts','unitee_files');

// Adding woocommerce support
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );



function unitee_features(){
    add_theme_support('title-tag');
}

add_action('after_setup_theme','unitee_features');




// Adds magnifine glass availabilty for products 
if( version_compare( $woocommerce->version, '3.0.0', ">=" ) ) {

	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );

	function avia_woocommerce_gallery_thumbnail_description($img, $attachment_id, $post_id, $image_class ) {
	    return $img;
	}
	function avia_woocommerce_post_thumbnail_description($img, $post_id){
	    return $img;
	}

}

// removing certain files when exporting wordpress file
add_filter('ai1wm_exclude_content_from_export', 'ignoreCertainFiles');

function ignoreCertainFiles($exclude_filters){
    $exclude_filters[] ='themes/unitee/node_modules';
    return $exclude_filters;
}