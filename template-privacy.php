<?php
// Template Name: Privacy Page

get_header();
   ?>
    
<!-- Frog landscape design as background picture -->
<div class="padding-top">
    <div style="background-image: url(<?php echo get_theme_file_uri('/images/Frog.png')?>);" class="privacy-policy">
    <div class="container ">
    <h1 class="privacy-policy__title"><?php the_title();?>: Our Promises</h1>
    <!-- Currently not using privacy policy banner board-->
    <!-- <div class="page-headers__privacy-policy" style="background-image: url(<?php echo get_theme_file_uri('/images/PrivacyPolicyBanner.png')?>)">
    
    </div> -->

      <h4 class="privacy-policy__headers" > How we use your information</h4>
      <div class="privacy-policy__sections">
        <p class="privacy-policy__paragraphs"> At Unitee, we are 100% committed to protecting your right to privacy. We totally understand 
           how vital online privacy is. When absolutley neccesasry we do use your personal information, such as your gender, date of birth, name, email address, address, phone number and shipping numbers. </p>
         
    </div>
<!-- Table to explain to user what data Unitee keep and why they keep it -->
      <table class="privacy-policy__table">
        <thead>
          <tr>
            <th>What we do</th>
            <th>Why we do it</th>
          </tr>
        <thead>
        <tbody> 
          <tr>
            <td>Deliver your purchases to you</td>
            <td>We need your name and contact details to send you our great Unitee products!</td>
          </tr>
          <tr>
            <td>Take payment, and give refunds</td>
            <td>After all - we’re not giving our stuff away for free!</td>
          </tr>
          <tr>
            <td>Keep a financial record of any transactions</td>
            <td>We need to keep track of what you have paid for. We have to keep track of payments our side to for tax purposes. It is a legal responsibility, we have to do this </td>
          </tr>
          <tr>
            <td>Send you account updates. For example, updates to order confirmations or our Terms & Conditions</td>
            <td>To keep you informed of any changes made to Unitee </td>
          </tr>
          <tr>
            <td>Allow you to track orders</td>
            <td>So you know when the delivery man is coming to drop off your order!</td>
          </tr>
        </tbody>
    </table>
<!-- Section explaining why they dont share information -->
    <h4 class="privacy-policy__headers" > Sharing your information</h4>

    <div class="privacy-policy__sections">
        
        <p class="privacy-policy__paragraphs"> We do not, and will never sell any of your personal data to any third party companies. This includes
           your name, address, email address or credit card information. We want to  maintain your trust and make sure you feel safe and secure when browsing our website.</p>
    </div>
    <!-- Explaining the rights the user has -->
    <h4 class="privacy-policy__headers" > Your rights</h4>
    <div class="privacy-policy__sections">
        
        <p class="privacy-policy__paragraphs"> You have a lot of rights regarding your personal information, these are: </p>
        <ul>
        <li>The right to be informed about how your personal information is being used</li>
        <li>The right to access the personal information we hold about you</li>
        <li>The right to request the correction of inaccurate personal information we hold about you (although you can probably do most of this through My Account</li>
        <li>The right to request that we delete your data, or stop processing it or collecting it, in some circumstances</li>
        <li>The right to complain to your data protection regulator — in the UK, the Information Commissioner’s Office</li>
        <li>The right to be told about any Security Breaches</li>
      </ul>
      <!-- Contact information - Currently the phone number is not their actual customer number -->
      <p class="privacy-policy__paragraphs">If you want to exercise your rights, have a complaint, or just have a question regarding anything, please contact us. We have 30 days in which to respond to you. Our phone number and email are listed at the end of this Policy.</p>
      <p class="privacy-policy__paragraphs"><strong>Email: </strong>Unitee@gmail.com</p>
      <p class="privacy-policy__paragraphs"><strong>Phone Number: </strong> 07732316568</p>

    </div>
  </div>
  </div>
  </div>

  <?php 
    get_footer();
  ?>  
  