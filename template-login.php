
<?php
// Template Name: Login Page

get_header();
 ?>
 <!-- This div makes content overflow into footer on smaller screens/mobile devices -->
<div style="display: block; overflow:auto; background-color:#ffd966;">
<!-- Used octupus illustration as background for account page -->
<div class="page-backgrounds__login padding-top" style="background-image: url(<?php echo get_theme_file_uri('/images/Octupus2.png')?>)" >

<!-- Login Board illustration - Currently not using
  <div style="text-align:center" class ="container ">  
  <img class="page-headers container" src ="<?php echo get_theme_file_uri('/images/Login2.png')?>"/>  
  </div> --> 

  <!-- Insert woocommerce content - account dashboard, login and registration functionality -->
    <div style="padding-top:50px;" class="checkout-page__products container">
    <!-- if statement, if current user is not logged in display the list below -->
    <?php if(!is_user_logged_in()){?>
<!-- With javascript and editing woocommerce core files. When click on login login form appears/register form dissapears, when click on register register form appeers/log in form dissapers  -->
<!-- Custom woocommerce css found in woocommerce.css file. Also core files in woocommerce folder were changed -->
        <ul class="account-links">
          <li class="account-links__login">Login</li>
          <li class="account-links__or">OR</li>
          <li class="account-links__register">Register</li>
        </ul>
        <?php }?>
  <!-- Add extra padding when logged in  -->
  <?php if(is_user_logged_in()){?>
    <div style="padding-top:3rem">
    </div>
  <?php }?>      
<!-- Woocommerce content -->
        <?php the_content();?>
    </div>
</div>
    </div>

<?php 
  get_footer();
?>

