<?php
  // Template Name: About-Us Page
  get_header();
?>
<!-- Page background image taken off one of Unitee's landscape design -->
<div class="padding-top">
<div style="background-image: url(<?php echo get_theme_file_uri('/images/AboutUsBottom.png')?>);position:relative" class="about-us" >


<!-- Using flex box display for about us page -->
<div class="about-us-container">
<!-- Conpany story explanation -->
<div class="about-us-section company-story">
    <div class ="company-story__col1" > 
        <div class="information t-left">
        <h1> The Story Behind Our Company</h1>
        <p class = "information__paragraph"> 
            Unitee is a start up clothing company set up by 3 graduate design students. The environment and sustainability is 
            something all three of us have cared about for a long time. The fashion industry is responsible for 10% of all Global emmissions and therefore
            we wanted to create a clothing company that was carbon neutral. All designs are created with the idea of animals and humans being able to work together to create a better world. 
            We hope you like our designs as much as we do!
            
            </p>
         </div>     
    </div>
    <!-- Image next to customer story - a new Unitee design -->
    <div class ="company-story__col2" >
        <img src="<?php echo get_theme_file_uri('images/DogsPlayingCards.png')  ?>" alt="dogsplayingCards" />

    </div>    
</div>
<!-- Unitee people section -->
<div class ="about-us-section unitee-people">
    <div class ="unitee-people__description">
        <!-- Unitee People Header - 100% width with brief introduction  -->
        <h1>The People Behind Unitee</h1>
        <p>Say hello to the three main guys that brought you Unitee. If you have any special requests you can always drop us a email! </p>
    </div>     
    <!-- Introducting the main 3 people behind Unitee. Unitee requested they are each assigned an animal. They specified what animal -->
    <div class ="unitee-people__col1"> 
        <!-- Image followed by person name and description -->
        <img src="<?php echo get_theme_file_uri('images/Kangaroo.png') ?>" alt="ArchieFordPicture"/>
        <h2 style="margin: 1rem 0">Archie Foord (The Kangaroo)</h2>
        <p>G'day mate! Our austalian designer. Did you know the first few Unitee desings were made on a powerpoint and a touchpad by this guy! He has now transitioned to an ipad and apple pen but still very impressive!</p>
    </div>
    <div class ="unitee-people__col2"> 
        <img src="<?php echo get_theme_file_uri('images/PolarBear.png') ?>" alt="TomCoulsonPicture"/>
        <h2 style="margin: 1rem 0">Tom Coulson (The Polar Bear)</h2>
        <p>The marketing manager. Reponsible for a few designs on this site, however his main role at Unitee is to maintain our depop site. He also loves to fish in his spare time </p>
    </div>
    <div class ="unitee-people__col3"> 
        <img src="<?php echo get_theme_file_uri('images/FrogBackground.png') ?>" alt="ChrisAndrewsPicture"/>
        <h2 style="margin: 1rem 0">Chris Andrews (The Frog)</h2>
        <p>Our money guy. Not a single payment transaction goes through without this guy knowing about it! If you want some artwork created by Chris check out the froggy.</p>
    </div>
</div>
<!-- Unitee Shirts standards section -->
<div class ="about-us-section unitee-shirts">
             <!-- Monkey skateboarding image -->
    <div class ="unitee-shirts__col1"> 
        <img src="<?php echo get_theme_file_uri('images/MonkeyGoingToSchool.png') ?>" alt="MonkeyGoingToSchool"/>
        
    </div>
    <div class ="unitee-shirts__col2"> 
        <h2>All Shirts made are eco friendly, Good Quality and made to last!</h2>
        <p style="line-height:2">
            As stated earlier all shirts produced by us do not contribute to greenhouse emmissions! Each shirt is designed to last, in years to come you will 
            still be rocking the same Unitee shirt you bought today! To top it off 20% of profits made by Unitee will go to various environmental charities.</p>
    </div>
   
</div>
</div>
</div>
</div>
</div>
<?php 
  get_footer();
?>
